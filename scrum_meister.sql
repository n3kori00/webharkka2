/* Tuhotaan tietokanta, mikäli se on olemassa. Samalla kaikki taulut tuhotaan. */
drop database if exists scrum_meister;

/* Luodaan tietokanta. */
create database if not exists scrum_meister;

/* Ennen taulujen luomista tulee sanoa, että käytetään äsken luotua tietokantaa. */ 
use scrum_meister;

/* Luodaan taulut. */
create table if not exists project (
    id int primary key auto_increment,
    name varchar(50) not null
);

create table if not exists sprint (
    id int primary key auto_increment,
    sprint_id varchar(10) not null,
    start date not null,
    end date not null,
    project_id int not null,
    constraint fk_project_id foreign key (project_id) references project(id)
    on delete restrict
);

create table if not exists person (
    id int primary key auto_increment,
    name varchar(100) not null,
    email varchar(100) not null unique,
    password varchar(255) not null   
);

create table if not exists project_person (
    id int primary key auto_increment,
    project_id int not null,
    constraint fk_project_person_project_id foreign key (project_id) references project(id)
    on delete restrict,
    person_id int not null,
    constraint fk_project_person_person_id foreign key (person_id) references person(id)
    on delete restrict    
);

create table if not exists task (
    id int primary key auto_increment,
    title varchar(50),
    sprint_id varchar(10),
    description text,
    project_id int not null,
    constraint fk_project_task_id foreign key (project_id) references project(id)
    on delete restrict
); 

create table if not exists task_person (
    id int primary key auto_increment,
    person_id int not null,
    constraint fk_person_task_id foreign key (person_id) references person(id)
    on delete restrict,
    task_id int not null,
    constraint fk_task_person_id foreign key (task_id) references task(id)
    on delete restrict
);

create table if not exists work (
    id int primary key auto_increment,
    duration float not null,
    description varchar(255) not null,
    task_id int not null,
    constraint fk_task_work_id foreign key (task_id) references task(id)
    on delete restrict,
    person_id int not null,
    constraint fk_person_work_id foreign key (person_id) references person(id)
    on delete restrict
);

create table if not exists activity (
    id int primary key auto_increment,
    message text not null,
    added timestamp default current_timestamp,
    project_id int not null,
    constraint fk_project_activity_id foreign key (project_id) references project(id)
    on delete restrict,
    person_id int not null,
    constraint fk_person_activity_id foreign key (person_id) references person(id)
    on delete restrict
);

insert into project (name) values ('Project_test');

insert into person (name,email,password) values ('Jouni Juntunen','jouni.juntunen@oamk.fi',md5('admin'));

