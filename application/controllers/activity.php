<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Activity extends MY_Controller{
    public function __construct() {
        parent::__construct();
        $this->load->model('activity_model');
    }
    
    public function index() {
        $this->session->set_userdata('active_tab',0);
        $data['activities']=$this->activity_model->get_all($this->session->userdata('project')->id);
        $this->load->view('activity_view',$data);
    }
    
    public function insert() {
        $data=array(            
            'message' => $this->input->post('message'),
            'project_id' => $this->session->userdata('project')->id,
            'person_id' => $this->session->userdata('user')->id
);        
        $this->activity_model->insert($data);        
        redirect('project/index','refresh');   
    }
    
    public function delete($id) {
        $this->activity_model->delete($id);
        redirect('project/index','refresh');  
    }
    
    public function update_feed() {
        $last_activity_id=$this->input->post('activity_id');
        $activities=$this->activity_model->get_all($this->session->userdata('project')->id,$last_activity_id,'asc');
        
        header('Content-type: application/json');        
        $json='[';
        
        foreach ($activities as $activity) {
            $json.="{";
            $json.='"person_name": "' .  $activity->person_name . '",';
            $json.='"added": "' .  $activity->added . '",';
            $json.='"activity_id": "' .  $activity->activity_id . '",';
            $json.='"activity_message": "' . $activity->message . '"';            
            $json.="},";    
        }

        $json=substr($json,0, strlen($json)-1);
        $json.=']';
        print $json;          
    }
}
