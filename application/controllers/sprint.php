<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sprint  extends MY_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('sprint_model');
        $this->load->model('task_model');
        $this->load->model('person_model');
        $this->load->model('task_person_model');
        $this->load->model('work_model');
    }    
    
    public function index() {        
        $this->session->set_userdata('active_tab',2);
        
        if (!empty($this->get_sprint())) {        
            $data['sprint']=$this->get_sprint();        
            $data['tasks']=$this->task_model->get_all($this->get_project()->id,$this->get_sprint()->sprint_id);            
        }
        else {
            $data['sprint']=NULL;        
            $data['tasks']=NULL;
        }
        
        $data['members']=$this->person_model->get_all();            
        $data['user']=$this->get_user();
        
        $this->load->view('sprint_plan_view',$data);
    }
    
    public function insert() {
        $data=array(
            'sprint_id' => $this->input->post('sprint_id'),
            'start' => $this->input->post('start'),
            'end' => $this->input->post('end'),
            'project_id' => $this->get_project()->id
        );
        
        $this->sprint_model->insert($data);
        redirect('project/index','refresh');        
    }
    
    public function assign() {
        $members=$this->input->post('member');
        foreach ($members as $member) {        
            $data=array(            
                'person_id' => $member,
                'task_id' => $this->input->post('selected_task_assign')          
            );        
        
            $this->task_person_model->insert($data);
        }
        redirect('project/index','refresh');        
    }
    
    public function deassign($task_person_id) {
        $this->task_person_model->delete($task_person_id);
        redirect('project/index','refresh');        
    }
    
    public function work() {
        $data=array(            
            'person_id' => $this->input->post('user_id'),
            'task_id' => $this->input->post('selected_task_work'),
            'duration' => $this->input->post('duration')
        );        
        $this->work_model->insert($data);
        redirect('project/index','refresh');        
    }
    
    public function remove_work($work_id) {
        $this->work_model->delete($work_id);
        redirect('project/index','refresh');        
    }
}