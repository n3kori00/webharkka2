<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report  extends MY_Controller{
    public function __construct() {
        parent::__construct();
        $this->load->model('report_model');
    }
    
    
    public function index() {
        $this->session->set_userdata('active_tab',3);
        
        if ($this->get_project()!=FALSE) {
            $data["report"]=$this->report_model->get_project_work_by_person($this->get_project()->id);
        }
        else {
            $data["report"]=NULL;
        }
        
        $this->load->view('reports/project_work_by_person',$data);
    }
}