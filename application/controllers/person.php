<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Scrum-Meister
 *
 * Scrum-projektinhallintaohjelmisto
 *
 * @package		Scrum-Meister
 * @author		Jouni Juntunen
 * @license		MIT 
 * @since		Version 1.0 
 */

// ------------------------------------------------------------------------

/**
 * Person-luokka.
 *
 * Henkilötietojen käsittelijä.
 *
 * @package		Scrum-Meister
 * @subpackage          Controllers
 * @category            Person
 * @author		Jouni Juntunen 
 */
class Person extends MY_Controller {
        
    public function __construct() {
        parent::__construct();
        $this->load->model('person_model');
        $this->load->model('project_person_model');
    }    
    
    // --------------------------------------------------------------------

    /**
     * index
     *
     * Metodi palauttaa listanäkymän henkilöistä.
     */    
    public function  index() {
        $this->session->set_userdata('active_tab',5);
        if ($this->get_project()!=FALSE) {
            $data['persons']=$this->person_model->get_all($this->get_project()->id);        
        }
        else {
            $data['persons']=NULL;        
        }
        $this->load->view('person_view',$data);
    }

    // --------------------------------------------------------------------

    /**
     * save
     *
     * Metodi lukee uuden henkilön tiedot lomakkeelta ja tallentaa tietokantaan.
     */
    public function insert() {
        $data=array(            
            'name' => $this->input->post('name'),
            'email' => $this->input->post('email'),
            'password' => md5($this->input->post('password')),            
        );
        
        $person_id=$this->person_model->insert($data);
        
        $data=array(
            'project_id' => $this->session->userdata('project')->id,
            'person_id' => $person_id
        );
        $this->project_person_model->insert($data);        
        redirect('project/index','refresh');        
    }
    
    public function update() {
        
        $data=array(            
            'id' => $this->input->post('id'),
            'name' => $this->input->post('name'),
            'email' => $this->input->post('email'),
            'password' => md5($this->input->post('password'))            
        );
        
        $this->person_model->update($data);
        redirect('project/index','refresh');        
    }
    
    // --------------------------------------------------------------------

    /**
     * delete
     *
     * Metodi poistaa henkilön ja ohjaa käyttäjän index-metodiin.
     * 
     * @param 	int	Poistettavan henkilön id.
     */
    public function delete($id) {
        $this->person_model->delete($id);
        redirect('project/index','refresh');        
    }
}
