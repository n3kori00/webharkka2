<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Backlog  extends MY_Controller{
    public function __construct() {
        parent::__construct();
        $this->load->model('task_model');        
    }
    
    public function index() {
        $this->session->set_userdata('active_tab',1);
        
        if ($this->get_project()!=FALSE) {
            $data['tasks']=$this->task_model->get_all($this->get_project()->id);
        }
        else {
            $data['tasks']=NULL;
        }
        $this->load->view('backlog_view',$data);
    }
    
    public function insert() {
        $data=array(            
            'title' => $this->input->post('title'),
            'description' => $this->input->post('description'),
            'project_id' => $this->session->userdata('project')->id            
        );
        
        $task_id=$this->task_model->insert($data);                
        redirect('project/index','refresh');        
    }
    
    public function update() {
        $data=array(            
            'id' => $this->input->post('id'),    
            'title' => $this->input->post('title'),
            'description' => $this->input->post('description'),
            'project_id' => $this->project->id            
        );
        
        $task_id=$this->task_model->update($data);                
        redirect('project/index','refresh');        
    }
    
    public function move_to_sprint($task_id) {                
        $data=array(            
            'id' => $task_id,    
            'sprint_id' => $this->get_sprint()->sprint_id
        );
        $this->task_model->update($data);                        
        redirect('project/index','refresh');        
    }
    
    public function delete($id) {        
        $this->task_model->delete($id);
        redirect('project/index','refresh');        
    }
}