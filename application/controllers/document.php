<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Document  extends MY_Controller{
    public function __construct() {
        parent::__construct();
        $this->load->helper('directory');
    }
    
    public function index() {        
        $this->session->set_userdata('active_tab',4);
        $data['files']=directory_map('./uploads');        
        $this->load->view('document_view',$data);    
    }
    
    public function do_upload() {
        $config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'gif|jpg|png|pdf|docx';
        $config['max_size']	= '5000';
        $config['max_width']  = '5000';
        $config['max_height']  = '3000';

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload())
        {
            //$error = array('error' => $this->upload->display_errors());            
            //redirect('project/index/5');
            print $this->upload->display_errors();
            
        }
        else
        {
            redirect('project/index','refresh');        
        }
    }
}