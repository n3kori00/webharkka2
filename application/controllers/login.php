<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of login
 *
 * @author jjuntune
 */
class Login extends CI_Controller{
    public function __construct() {
        parent::__construct();
        $this->load->model('person_model');
    }
    
    public function index() {
        $this->load->view('login_view.php');
    }
    
    public function validate() {        
        $person=$this->person_model->get($this->input->post('email'),md5($this->input->post('password')));
        
        if (empty($person)) {
            $this->session->set_userdata('active_tab',0);
            $this->index();
        }
        else {
            $this->session->set_userdata('user',$person);
            redirect('project/index');
        }
    }
    
    public function logout() {        
        $this->session->unset_userdata('user');
        $this->session->sess_destroy();
        redirect('login/index');
    }
}
