<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Scrum-Meister
 *
 * Scrum-projektinhallintaohjelmisto
 *
 * @package		Scrum-Meister
 * @author		Jouni Juntunen
 * @license		MIT 
 * @since		Version 1.0 
 */

// ------------------------------------------------------------------------

/**
 * Person_model luokka.
 *
 * Henkilötietojen malli.
 *
 * @package		Scrum-Meister
 * @subpackage          Models
 * @category            Person
 * @author		Jouni Juntunen 
 */
class Person_model extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->load->model('task_person_model');
        $this->load->model('work_model');
    }
    
    // --------------------------------------------------------------------

    /**
     * get_all
     *
     * Metodi palauttaa kaikki henkilöt tietokannasta.
     * 
     * @return  object  Haetut henkilöt.
     */
    public function get_all() {           
        $query=$this->db->get('person');
        return $query->result();
    }

    public function get($email,$password) {              
        $this->db->where('email',$email);
        $this->db->where('password',$password);
        $query=$this->db->get('person');
        return $query->row(); 
    }
    
    
    public function check_user($email,$password) {
        $user=$this->get($email,$password);
        if (!empty($user)) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }
    // --------------------------------------------------------------------

    /**
     * save
     *
     * Metodi lisää uuden henkilön tietokantaan.
     * 
     * $param   array   Lisättävän henkilön tiedot taulukossa.
     * @return  int     Tietokantaan lisätyn tietueen id.
     */
    public function insert($data) {
        $this->db->insert('person',$data);        
        return $this->db->insert_id();
    }
    
    public function update($data) {
        $this->db->where('id',$data['id']);
        $this->db->update('person',$data);
    }
    
    // --------------------------------------------------------------------

    /**
     * delete
     *
     * Metodi poistaa henkilön tietokannasta. Samalla poistetaan kaikki henkilöön
     * liittyvä tieto.
     * 
     * @param   int     Poistettavan tietueen id.
     */
    public function delete($id) {
        $this->work_model->delete_task_work($id);
        $this->task_person_model->delete_task_person($id);
        $this->db->where('person_id',$id);
        $this->db->delete('project_person');
        $this->db->where('id',$id);
        $this->db->delete('person');
    }
}
