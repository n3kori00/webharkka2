<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of project_model
 *
 * @author jjuntune
 */
class Project_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
      
    public function get_all() {
        $query=$this->db->get('project');
        return $query->result();
    }
    
    public function get($id) {
        $this->db->where('id',$id);
        $query=$this->db->get('project');
        return $query->row();
    }
    
    public function get_first() {
        $this->db->order_by('id','asc');
        $this->db->limit(1);
        $query=$this->db->get('project');
        return $query->row();
    }
    
    public function insert($data) {
        $this->db->insert('project',$data);
        return $this->db->insert_id();
    }
    
    public function delete($id) {
         $this->db->where('id',$id);
         $this->db->delete('project');
    }
}