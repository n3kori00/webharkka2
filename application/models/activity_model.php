<?php

class Activity_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    
    public function get_all($project_id=0,$last_activity_id=0,$order_by='desc') {        
        $this->db->select('person.name as person_name,activity.id as activity_id,activity.added,activity.message');
        $this->db->from('person');
        $this->db->join('activity','activity.person_id=person.id');        
        
        
        if ($project_id>0) {
            $this->db->where('project_id',$project_id);
        }
        
        
        if ($last_activity_id>0) {
            $this->db->where('activity.id > ',$last_activity_id);
        }
        
        $this->db->order_by('activity.added',$order_by);
        $query=$this->db->get();
        
        return $query->result();
    }
 
    public function delete($id) {
        $this->db->where('id',$id);
        $this->db->delete('activity');
    }
    
    public function insert($data) {
        $this->db->insert('activity',$data);        
        return $this->db->insert_id();
    }
    

    
}

