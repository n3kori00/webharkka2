<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of report_model
 *
 * @author jjuntune
 */
class Report_model extends CI_Model{
    public function __construct() {
        parent::__construct();
    }
    
    public function get_project_work_by_person($project_id) {
        $this->load->model('work_model');
        
        $this->db->select('project.id as project_id,project.name as project_name,sprint.sprint_id,task.id as task_id,task.title,person.name as person_name,task_person.person_id');
        $this->db->from('project');
        $this->db->join('sprint','project.id=sprint.project_id');                
        $this->db->join('task','sprint.sprint_id=task.sprint_id');               
        $this->db->join('task_person','task.id=task_person.task_id');                       
        $this->db->join('person','task_person.person_id=person.id');               
        $this->db->where('project.id = ',$project_id);        
        $this->db->order_by('sprint.sprint_id');
        $this->db->order_by('task.id');
        $query=$this->db->get();                
        
        $report=$query->result();
        
        foreach ($report as $r) {            
            $work=$this->work_model-> get_by_task_and_person($r->task_id,$r->person_id);
            $total=0;
            foreach ($work as $w) {
                $total+=$w->duration;
            }
            $r->duration=$total;
        }
        
        return $report;                
    }
}
