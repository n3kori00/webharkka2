<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Task_model extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->load->model('task_person_model');
        $this->load->model('work_model');
    }
      
    public function get_all($project_id=0,$sprint_id='') {        
        if ($project_id>0) {
            $this->db->where('project_id',$project_id);
        }
        if (strlen($sprint_id)>0) {
            $this->db->where('sprint_id',$sprint_id);
        }
        $query=$this->db->get('task');
        $tasks=$query->result();
        if (strlen($sprint_id)>0) {
            foreach ($tasks as $task) {
                $task->members=$this->task_person_model->get_all($task->id);
                $task->work=$this->work_model->get_all($task->id);
            }
        }
        return $tasks;        
    }
    
    public function insert($data) {
        $this->db->insert('task',$data);
        return $this->db->insert_id();
    }
    
    public function update($data) {     
        $this->db->where('id',$data['id']);
        $this->db->update('task',$data);        
    }
    
    public function delete($id) {
        $this->work_model->delete_task_work($id);
        $this->task_person_model->delete_task_person($id);
        $this->db->where('id',$id);
        $this->db->delete('task');
    }
}
