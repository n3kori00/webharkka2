<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of task_person_model
 *
 * @author jjuntune
 */
class Task_person_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    
    public function get_all($task_id) {        
        $this->db->select('task_person.id,person.name');
        $this->db->from('person');
        $this->db->join('task_person','task_person.person_id=person.id');                
        $this->db->where('task_person.task_id = ',$task_id);        
        $query=$this->db->get();        
        
        return $query->result();
    }
    
    public function insert($data) {
        $this->db->insert('task_person',$data);        
        return $this->db->insert_id();
    }
    
    public function delete($id) {
         $this->db->where('id',$id);         
         $this->db->delete('task_person');
    }
    
    public function delete_task_person($task_id) {
         $this->db->where('task_id',$task_id);
         $this->db->delete('task_person');
    }
}
