<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of project_person
 *
 * @author jjuntune
 */
class Project_person_model extends CI_Model{
    public function __construct() {
        parent::__construct();
    }
    
    public function insert($data) {
        $this->db->insert('project_person',$data);        
        return $this->db->insert_id();
    }
}
