<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of work_model
 *
 * @author jjuntune
 */
class Work_Model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
      
    public function get_all($task_id) {
        $this->db->select('work.*,person.name as person_name');        
        $this->db->from('work');
        $this->db->join('person','work.person_id=person.id');               
        $this->db->where('task_id',$task_id);                
        $query=$this->db->get();                
        return $query->result();
    }    
    
    public function get_by_task_and_person($task_id,$person_id) {        
        $this->db->select('work.*,person.name as person_name');        
        $this->db->from('work');
        $this->db->join('person','work.person_id=person.id');               
        $this->db->where('task_id',$task_id);  
        $this->db->where('person_id',$person_id);  
        $query=$this->db->get();                
        return $query->result();
    }
    
    public function insert($data) {
        $this->db->insert('work',$data);
        return $this->db->insert_id();
    }    
    
    public function delete($id) {
         $this->db->where('id',$id);
         $this->db->delete('work');
    }
    
    public function delete_task_work($task_id) {
         $this->db->where('task_id',$task_id);
         $this->db->delete('work');
    }
}
