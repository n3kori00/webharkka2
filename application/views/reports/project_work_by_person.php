<table class='list'>
    <tr>
        <th>Project</th>
        <th>Sprint</th>
        <th>Task</th>
        <th>Assigned</th>
        <th>Duration</th>
    </tr>
<?php
$project_id=0;
$sprint_id="";
$task_id=0;
$sprint_work=0;
$project_total=0;
if ($report!=NULL) {
    foreach ($report as $r) {

        if ($sprint_id!=$r->sprint_id && $sprint_work>0) {
            print "<tr><td></td><td></td><td></td><td style='font-weight: bold;'>Sprint total:</td><td>$sprint_work</td></tr>";
            $sprint_work=0;
        }    

        print "<tr>";
        if ($project_id!=$r->project_id) {
            print "<td style='font-weight: bold;'>" . $r->project_name ."</td>";
        }
        else {
            print "<td></td>";
        }
        if ($sprint_id!=$r->sprint_id) {    
            print "<td>" . $r->sprint_id ."</td>";
        }
        else {
            print "<td></td>";
        }

        if ($task_id!=$r->task_id) {
            print "<td>" . $r->title ."</td>";
        }
        else {
            print "<td></td>";
        }

        print "<td>" . $r->person_name . "</td>";           
        print "<td>";   
        print $r->duration;       
        $sprint_work+=$r->duration;
        $project_total+=$r->duration;
        print "</td>";    
        print "</tr>";

        $project_id=$r->project_id;
        $sprint_id=$r->sprint_id;
        $task_id=$r->task_id;
    }
    if ($sprint_work>0) {
        print "<tr><td></td><td></td><td></td><td style='font-weight: bold;'>Sprint total:</td><td>$sprint_work</td></tr>";    
    }

    if ($project_total>0) {
        print "<tr><td></td><td></td><td></td><td style='font-weight: bold;'>Project total:</td><td>$project_total</td></tr>";
    }
}
?>
</table>