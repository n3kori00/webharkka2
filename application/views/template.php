<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet" type="text/css">        
        <link <?php echo 'href="' . base_url() . 'application/css/style.css"';?> type="text/css" rel="stylesheet" />
        <script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>        
        
        <script>
        $(function() {                        
            $.ajaxSetup({
                cache: false
            });
            
            $( "#tabs" ).tabs( {
                active: <?php echo $active_tab;?>
            });                                    
            
            
            var project_dialog=$( "#project_dialog" ).dialog({
                autoOpen: false,
                height: 175,
                width: 350,
                modal: true
            });                        
            
            sprint_dialog=$( "#sprint_dialog" ).dialog({
                autoOpen: false,
                height: 300,
                width: 350,
                modal: true
            });
            
            settings_dialog=$( "#settings_dialog" ).dialog({
                autoOpen: false,
                height: 150,
                width: 350,
                modal: true
            });
            
            user_dialog=$( "#user_dialog" ).dialog({
                autoOpen: false,
                height: 340,
                width: 360,
                modal: true
            });
            
            
            $( "#add_project").click(function() {
                project_dialog.dialog("open");
            });
            
            $( "#close_project").click(function() {
                project_dialog.dialog("close");
            }); 
            
            $( "#add_sprint").click(function() {
                sprint_dialog.dialog("open");
            });
            
            $( "#close_sprint").click(function() {
                sprint_dialog.dialog("close");
            }); 
            
            $( "#show_user").click(function() {
                user_dialog.dialog("open");
            });
            
            $( "#close_user").click(function() {
                user_dialog.dialog("close");
            }); 
            
            $( "#show_settings").click(function() {
                settings_dialog.dialog("open");
            });
            
            $( "#close_settings").click(function() {
                settings_dialog.dialog("close");
            }); 
            
        });        
        </script>
    </head>
    <body>
       
       <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Scrum-Meister</a>
                </div>
                <div id="navbar" class="collapse navbar-collapse">                    
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            <?php 
                            if ($project!=NULL) {
                                print $project->name;
                            }
                            else {
                                print "No project";
                            }
                            ?>
                            <span class="caret"></span></a>                            
                            <ul class="dropdown-menu" role="menu">
                                <?php
                                if ($project!=NULL) {
                                    foreach ($projects as $project) {
                                        print "<li><a href='" . site_url() . 'project/change_project/' .  $project->id  . "'>" . $project->name . "</a></li>";
                                    }
                                }
                                ?>                                
                                <li><a id="add_project" href="#">Add new...</a></li>
                            </ul>                            
                        </li>                       
                        
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">
                                <?php 
                                if ($sprint!=NULL) {
                                    print $sprint->sprint_id; 
                                }
                                else {
                                    print "No sprints";
                                }
                                ?>
                                
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <?php
                                if ($sprint!=NULL) {
                                    foreach ($sprints as $sprint) {
                                        print "<li><a href='" . site_url() . 'project/change_sprint/' . $sprint->id  . "'>" .  $sprint->sprint_id  ."</a></li>";
                                    }
                                }
                                ?>
                                <li><a id="add_sprint" href="#">Add new...</a></li>
                            </ul>
                        </li>                                               
                        <li><a id="show_user" href="#"><?php print $user->name;?>&nbsp;<span class="glyphicon glyphicon-user"></span></a></li>
                        <li><a id="show_settings" href="#"><span class="glyphicon glyphicon-cog"></span></a></li>
                        <li><a href="<?php print site_url();?>login/logout"><span class="glyphicon glyphicon-off"></span></a></li>                        
                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </nav>
        <div class="container" style="margin-top: 70px;">            
            <?php 
            $data['active_tab']=$active_tab;
            $data['user']=$user;
            $this->load->view('project_view',$data);
            $this->load->view('sprint_view',$data);
            $this->load->view('settings_view',$data);
            $this->load->view('user_view',$data);                        
            ?>                
            <div class="col-md-12">                                         
                <div class="row">                                
                    
                    <div id="tabs">
                        <ul>
                            <li><a href="<?php echo site_url() . 'activity/'?>">Activity</a></li>
                            <li><a href="<?php echo site_url() . 'backlog/'?>">Backlog</a></li>
                            <li><a href="<?php echo site_url() . 'sprint/'?>">Sprint</a></li>
                            <li><a href="<?php echo site_url() . 'report/'?>">Report</a></li>
                            <li><a href="<?php echo site_url() . 'document/'?>">Document</a></li>
                            <li><a href="<?php echo site_url() . 'person/'?>">Members</a></li>
                        </ul>                
                        <div id="tabs-1">                        
                        </div>
                        <div id="tabs-2">                        
                        </div>
                        <div id="tabs-3">                        
                        </div>
                        <div id="tabs-4">                        
                        </div>
                        <div id="tabs-5">                        
                        </div>
                        <div id="tabs-6">                        
                        </div>
                    </div>
                </div>             
            </div>
        </div>
    </body>
</html>