<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Scrum-meister - Login</title>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">        
        <link <?php echo 'href="' . base_url() . 'application/css/style.css"';?> type="text/css" rel="stylesheet" />
        <script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>        
        <style>
            label {
                display: block;
            }
        </style>
    </head>
    <body>
        <script>
            $(function() {
                $( "#login_dialog" ).dialog({
                    autoOpen: true,
                    height: 220,
                    width: 400,
                    modal: true
                });
            });        
        </script>        
        <div id="login_dialog" title="Scrum-Meister - Login">
            <form method="post" action="<?php print(site_url());?>login/validate">                
                <label>Email:</label>
                <input name="email" maxlength="100" size="30" type="email">

                <label>Password:</label>
                <input name="password" maxlength="20" size="30" type="password">
                
                <div class="buttons">
                    <input type="submit" value="Login">            
                </div>
            </form>
        </div
    </body>
</html>