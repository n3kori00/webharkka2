<script>
$(function() {   
    $( ".save_assignment").click(function() {                
        $ (this ).parent().submit();        
    });        
    
    $( ".close_assignment").click(function() {                
        $ (this ).parent().css('display','none');        
    });        
    
    $( " .assign_task").click(function() {       
        $( this ).next().css('display','block');        
    });

    $( ".save_work").click(function() {                
        $ (this ).parent().submit();        
    });        
    
    $( ".close_work").click(function() {                
        $ (this ).parent().css('display','none');        
    });        
    
    $( " .add_work").click(function() {       
        $( this ).next().css('display','block');        
    });

});
</script>
<p>
Selected sprint: 
<?php  
if ($sprint!=NULL) {
    print $sprint->sprint_id;
}
else {
    print "No sprints";
}
?>
</p>
<table class="list">
    <tr>
        <th class="id"></th>
        <th>Title</th>
        <th>Description</th>
        <th>Assigned</th>        
        <th>Work</th>                
    </tr>
<?php
if ($tasks!=NULL) {
    foreach ($tasks as $task) {
        print "<tr>";
        print "<td class='id'>" . $task->id .  "</td>";
        print "<td>" . $task->title .  "</td>";
        print "<td>" . $task->description . "</td>";    
        print "<td class='list_text'>";    
        foreach ($task->members as $member) {
            print $member->name;
            print "<a href='" . site_url() . "sprint/deassign/$member->id'>";
            print "&nbsp;<img src='" . site_url() . "application/images/remove.png'>";
            print "</a>&nbsp;";
        }    
        print "<a class='assign_task sprint_table_text'><img src='" . site_url() .  "application/images/add.png'></a>";        

        print "<form method='post' action='" . site_url() . "sprint/assign' class='assign_form'>";
        print "<input type='hidden'  name='selected_task_assign' value='$task->id'>";
        foreach ($members as  $member) {
                print "<input type='checkbox' name='member[]' value='$member->id'>$member->name<br />";            
        }           
        print "<a href='#' class='save_assignment'>Save</a> | ";
        print "<a href='#' class='close_assignment'>Close</a>";
        print "</form>";        

        print "</td>";
        print "<td class='list_text'>";
        $total=0;

        $remove_links="";
        foreach ($task->work as $w) {        
            $total+=$w->duration;        
            $remove_links.=$w->duration . "&nbsp;";                        
            $remove_links.=$w->person_name;
            $remove_links.= "<a href='" . site_url() . "sprint/remove_work/$w->id'>";
            $remove_links.="<img src='" . site_url() . "application/images/remove.png'>&nbsp;";
            $remove_links.="</a><br />";
        }
        print"<a>" . $total . "</a>&nbsp;";
        print "<a class='add_work sprint_table_text'><img src='" . site_url() .  "application/images/add.png'></a>";

        print "<form method='post' action='" . site_url() . "sprint/work' class='work_form'>";                
        print $remove_links;
        print "<input type='number' id='duration' name='duration' step='any'>";    
        print "<input type='hidden' id='user_id' name='user_id' value='" . $user->id . "'>";
        print "<input type='hidden'  name='selected_task_work' value='$task->id'><br />";
        print "<a href='#' class='save_work'>Save</a> | ";
        print "<a href='#' class='close_work'>Close</a>";
        print "</form>";    
        print "</td>";                
        print "</tr>";
    }
}
?>
</table>