<script>
$(function() {
       function update() {
           
           activity_id = $("#last_activity_id").val();
           $.ajax({
               type: "POST",
               url: "<?php print site_url();?>activity/update_feed",
               data: {"activity_id":activity_id},
               success: function (output) {
                   $.each(output,function(i,v) {
                    $( '#form').after(
                            "<div class='message'>"  + 
                            "<p class='author'>by " + v.person_name + " " +
                            v.person_name + " " + v.added + " " +
                            "<a href='<?php print site_url();?>activity/delete/'" + v.activity_id + 
                            " onclick='return confirm(\"Delete message?\");'" + 
                            "'>" +                                                         
                            "<span class='glyphicon glyphicon-trash'></span></a>" +
                            "</p>" + 
                            v.activity_message  + 
                            "</div>"
                    );              
                    $(' #last_activity_id').val(v.activity_id);
                });
            }
        });
    }
        
        
    setInterval(update,5000);    
        
        
        
        $('#message').keypress(function (e) {
        if (e.which === 13) {
            $('#form').submit();
            return false;  
        }
    });
});

</script>

<form id="form" action="<?php print(site_url());?>activity/insert" method="post">
    <textarea id="message" name="message" placeholder="Add your message here and press enter"></textarea>
    <div class="buttons">    
    </div>
</form>

<?php

$count=0;

foreach ($activities as $activity) {
    
    
    if ($count==0) {
        print "<input id='last_activity_id' type='hidden' value='$activity->activity_id' name='last_activity_id'>";
    }
    
        print "<div class='message' id='$activity->activity_id'>";
        print "<p class='author'>by $activity->person_name&nbsp;$activity->added&nbsp;";
        print "<a href='" . site_url() . "activity/delete/" .  $activity->activity_id . "' onclick='return confirm(\"Delete message?\");'>";
        print "<span class='glyphicon glyphicon-trash'></span></a>";
        print "</p>";
        print $activity->message;    
        print "</div>";

        $count++;
}

