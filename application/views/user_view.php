<div id="user_dialog" title="User">
    <form method="post" action="<?php print(site_url());?>person/update">        
        <label>Name</label>
        <input name="name" value="<?php print $user->name;?>" maxlength="100" size="35">
        <label>Email</label>
        <input name="email" value="<?php print $user->email;?>" maxlength="100" size="35">
        <label>Old password</label>
        <input name="old_password" type="password" placeholder="Leave blank if password is not changed" maxlength="20" size="35">        
        <label>New password (max 20 chars)</label>
        <input name="new_password" type="password" placeholder="Leave blank if password is not changed" maxlength="20" size="35">        
        <div class="buttons">
            <input type="submit" value="Save">
            <a id="close_user">Close</a>
        </div>
    </form>
</div> 