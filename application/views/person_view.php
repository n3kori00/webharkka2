<script>
$(function() {    
    $( "#add_member" ).click(function() {
        $( "#member_form_insert" ).show( "slow", function() {    
        });
    });        
    
    $( "#close_member" ).click(function() {
        $( "#member_form_insert" ).hide( "slow", function() {    
        });
    });    
});      
</script>
<a id="add_member">Add new member </a>
<div id="member_form_insert">
    <form action="<?php print(site_url());?>person/insert" method="post">
        <div>
        <label>Name:</label>
        <input name="name" maxlength="100" size="30">
        </div>
        <div>
        <label>Email:</label>
        <input name="email" maxlength="100" type="email" size="30">
        </div>
        <div>
        <label>Password:</label>
        <input name="password" maxlength="20" type="password" size="20" placeholder="max 20 chars">
        </div>
        <div class="buttons">
            <input type="submit" value="Save">
            <a id="close_member" href="#">Close</a>
        </div>
    </form>
</div>
<form id="member_form_update" action="<?php print site_url() . "person/update/"; ?>" method="post">
<table class="list">
    <tr>
        <!--<th class="id"></th>-->
        <th>Name</th>
        <th>Email</th>
        <th></th>
        <th></th>
    </tr>
<?php
if ($persons!=NULL) {
    foreach ($persons as $person) {
        print "<tr>";    
        print "<td>" . $person->name .  "</td>";
        print "<td>" . $person->email . "</td>";    
        print "<td><a href='" . site_url() . "person/delete/" .  $person->id . "' onclick='return confirm(\"Delete member?\");'>"
                . "<span class='glyphicon glyphicon-trash'></span></a></td>";
        print "</tr>";
    }
}
?>
</table>
</form>