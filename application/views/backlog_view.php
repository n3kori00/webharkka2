<script>
$(function() {    
    $( "#add_task" ).click(function() {        
        $( "#task_form_insert" ).show( "slow", function() {    
        });
    });        
    
    $( "#close_task" ).click(function() {
        $( "#task_form_insert" ).hide( "slow", function() {    
        });
    });
    
    $( ".edit_task" ).click(function() {              
       var par = $(this).parent().parent();       
       var tdId=par.children("td:nth-child(1)");
       var tdTitle=par.children("td:nth-child(2)");
       var tdDescription=par.children("td:nth-child(3)");
       
       tdId.html("<input type='text' id='id' name='id' value='" + tdId.html() + "'>");
       tdTitle.html("<input type='text' id='title' name='title' value='" + tdTitle.html() + "'>");
       tdDescription.html("<textarea id='description' name='description'>" + tdDescription.html() + "</textarea>");       
       
       $( ".save_task").css('display','inline');
       $( ".cancel_task").css('display','inline');
       $( ".list .glyphicon-pencil").css('display','none');
    });
    
    $( ".save_task ").click(function() {
        $( "#task_form_update").submit();
    });
    
    $( ".cancel_task ").click(function() {
       var par = $(this).parent().parent();       
       var tdId=par.children("td:nth-child(1)");
       var tdTitle=par.children("td:nth-child(2)");
       var tdDescription=par.children("td:nth-child(3)");
       
       var id=$('#id').val();
       var title=$('#title').val();
       var description=$('#description').val();
       
       tdId.html(id);
       tdTitle.html(title);
       tdDescription.html(description);       
       
       $( ".save_task").css('display','none');
       $( ".cancel_task").css('display','none');
       $( ".list .glyphicon-pencil").css('display','inline');
    });
});      
</script>
<a id="add_task">Add task</a>
<div id="task_form_insert">
    <form action="<?php print(site_url());?>backlog/insert" method="post">
        <div>
        <label>Title:</label>
        <input name="title" maxlength="50" size="30">
        </div>
        <div>
        <label>Description:</label>
        <textarea name="description"></textarea>
        </div>        
        <div class="buttons">
            <input type="submit" value="Save">
            <a id="close_task">Close</a>
        </div>
    </form>
</div>
<form id="task_form_update" action="<?php print site_url() . "backlog/update/"; ?>" method="post">
<table class="list">
    <tr>
        <th class="id"></th>
        <th>Title</th>
        <th>Description</th>
        <th></th>
        <th></th>
        <th></th>
    </tr>
<?php
if ($tasks!=NULL) {
    foreach ($tasks as $task) {
        print "<tr>";
        print "<td class='id'>" . $task->id .  "</td>";
        print "<td>" . $task->title .  "</td>";
        print "<td>" . $task->description . "</td>";
        print "<td>";
        print "<a class='edit_task'><span class='glyphicon glyphicon-pencil'></span></a>";    
        print "<a class='save_task'>Save | </a>";    
        print "<a class='cancel_task'>Cancel</a></td>";    
        print "<td><a href='" . site_url() . "backlog/move_to_sprint/" .  $task->id . "'>"
                . "<span class='glyphicon glyphicon-th-list'></span></a></td>";    
        print "</td>";
        print "<td><a href='" . site_url() . "backlog/delete/" .  $task->id . "' onclick='return confirm(\"Delete task?\");'>"
                . "<span class='glyphicon glyphicon-trash'></span></a></td>";
        print "</tr>";
    }
}
?>
</table>
</form>