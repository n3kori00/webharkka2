<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MY_Controller
 *
 * @author jjuntune
 */
class MY_Controller extends CI_Controller {
    protected $project=NULL;
    protected $sprint=NULL;
    protected $user=NULL;

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata("user")==FALSE) {
            redirect('login/index','refresh');
        }        
    }    
    
    public function set_project($value) {
        $this->session->set_userdata('project',$value);
    }    
    
    public function get_project() {
        return $this->session->userdata('project');
    }
    
    public function set_sprint($value) {
        $this->session->set_userdata('sprint',$value);
    }
    
    public function get_sprint() {
        return $this->session->userdata('sprint');
    }
    
    public function set_user($value) {
        $this->session->set_userdata('user',$value);
    }
    
    public function get_user() {
        return $this->session->userdata('user');
    }    
}
